package com.implementation.timber;

import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

public class ReleaseTree extends Timber.Tree {

    private int MAX_LOG_LENGTH = 4000;

    /**
     * This method return True for WARN, ERROR, WTF
     *
     * @param tag
     * @param priority
     * @return
     */
    @Override
    protected boolean isLoggable(@Nullable String tag, int priority) {

        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
            return false;
        }
        //WARN ERROR WTF
        return true;
    }

    @Override
    protected void log(int priority, @Nullable String tag, @NotNull String message, @Nullable Throwable t) {
        if (isLoggable(priority)) {

            //Report the log Exception
            if (priority == Log.ERROR && t != null) {
                //Crashlytics.log(e)
            }

            if (message.length() < MAX_LOG_LENGTH) {
                if (priority == Log.ASSERT) {
                    Log.wtf(tag, message);
                } else {
                    Log.println(priority, tag, message);
                }

                return;
            }

            //Split by line, each line can fit into Log's maximum length
            for (int i = 0, length = message.length(); i < length; i++) {
                int newLine = message.indexOf('\n',i);
                newLine  = newLine != -1 ? newLine : length;

                do{
                    int end = Math.min(newLine, i+MAX_LOG_LENGTH);
                    String part = message.substring(i ,end);
                    if(priority == Log.ASSERT){
                        Log.wtf(tag, part);
                    }
                    else{
                        Log.println(priority, tag, part);
                    }
                }while (newLine < i+MAX_LOG_LENGTH);
            }
        }
    }
}
