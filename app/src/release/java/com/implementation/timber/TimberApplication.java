package com.implementation.timber;

import android.app.Application;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

public class TimberApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();


        Timber.plant(new ReleaseTree());

    }
}
